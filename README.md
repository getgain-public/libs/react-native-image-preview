# react-native-image-preview
Display zoomed image in transparent modal window

## Demo
![](http://i.imgur.com/lekjfbC.gif)

## Installation

```
npm i --save git@gitlab.com:getgain-public/libs/react-native-image-preview.git
```

## Usage

```Javascript
import ImagePreview from 'react-native-image-preview';

...

<ImagePreview visible={visible} source={{uri: 'some-source'}} close={setVisibleToFalse} />

```

## License
*MIT*
