import React from "react";
import {
  Modal,
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  ImageSourcePropType,
  TouchableOpacity,
  Text,
  ImageStyle,
  ViewStyle,
  StyleProp,
  Image,
} from "react-native";

type Props = {
  source: ImageSourcePropType;
  visible: boolean;
  close: () => void;
  imageStyle?: ImageStyle;
  overlayStyle?: ViewStyle;
  showCloseButton?: boolean;
  closeButtonStyles?: StyleProp<ViewStyle>;
};

export default function ImagePreview(props: Props) {
  const {
    source,
    visible,
    close,
    imageStyle,
    overlayStyle,
    showCloseButton = false,
    closeButtonStyles = {},
  } = props;

  return (
    <Modal
      animationType={"fade"}
      transparent={true}
      onRequestClose={close}
      visible={visible}
    >
      <View style={[styles.overlay, overlayStyle]}>
        <TouchableWithoutFeedback onPress={close}>
          <Image
            resizeMode={"contain"}
            source={source}
            style={[styles.image, imageStyle]}
          />
        </TouchableWithoutFeedback>
        {showCloseButton && (
          <TouchableOpacity
            style={[styles.closeButton, closeButtonStyles]}
            onPress={close}
          >
            <Text style={styles.closeButtonIcon}>{"\u2715"}</Text>
          </TouchableOpacity>
        )}
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.7)",
  },
  image: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    alignSelf: "center",
  },
  closeButton: {
    flex: 1,
    position: "absolute",
    width: 24,
    height: 26,
    borderRadius: 4,
    backgroundColor: "#01204B",
    alignSelf: "flex-end",
    top: "21%",
    right: "20.5%",
    justifyContent: "center",
  },
  closeButtonIcon: {
    fontSize: 14,
    alignSelf: "center",
    color: "white",
  },
  loader: {
    flex: 1,
    alignSelf: "center",
  },
});
